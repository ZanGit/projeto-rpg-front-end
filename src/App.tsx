import { useEffect } from "react";
import AuthProvider from "./contexts/authContext";
import AppRoutes from "./routes/routes";
import { uploadError } from "./contexts/firebase/database";

function App() {
  useEffect(() => {
    window.onerror = (message, source, lineno, colno, error) => {
      console.log("error", { message, source, lineno, colno, error });
      uploadError({
        message,
        source,
        lineno,
        colno,
        error,
      });
    };
  }, []);
  return (
    <AuthProvider>
      <AppRoutes />
    </AuthProvider>
  );
}

export default App;
